using System.ComponentModel.DataAnnotations.Schema;
namespace api.Models
{
    public class DemandeEntity
    {
        public int Id { get; set; }

        [ForeignKey(nameof(TaregetUserEntity))]
        public int? TaregetUserEntityId { get; set; }
        
        [ForeignKey(nameof(InitiatorUserEntity))]
        public int? InitiatorUserEntityId { get; set; }
        
        public UserEntity? TaregetUserEntity { get;set; }
        public UserEntity? InitiatorUserEntity { get;set; }
    }
}