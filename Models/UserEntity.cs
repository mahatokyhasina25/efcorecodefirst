using System.ComponentModel.DataAnnotations.Schema;
namespace api.Models
{
    public class UserEntity
    {
        public int Id { get; set; }
        [InverseProperty(nameof(DemandeEntity.InitiatorUserEntity))]
        public List<DemandeEntity> InitiatorUserEntity { get; set; }
        [InverseProperty(nameof(DemandeEntity.TaregetUserEntity))]
        public List<DemandeEntity> DemandeEntityTaregetUserEntity { get; set; }
    }
}