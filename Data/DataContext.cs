using Microsoft.EntityFrameworkCore;
using api.Models;
namespace api.Data
{
    public class DataContext : DbContext
    {
        
        public DataContext()
        {
        }

        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {
        }

        public DbSet<DemandeEntity> DemandeEntity { get; set; }
        public DbSet<UserEntity> UserEntity { get; set; }


    }

}